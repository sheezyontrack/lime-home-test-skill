## Task #1: Writing Test Scenarios

### Test Scenarios

#### Scenario: Access to booking information from Landing Page
+ Given that a user accesses the website of Lime Home
+ When the user is on the landing page
+ When the user provides the last name 
+ When the user provides a 10 digit alphanumeric booking reference code
+ When the information provided are valid as in database
+ When the user submits the form
+ Then the user is navigated away from the landing page to the check-in page where the full booking information is displayed

#### Scenario: Check-in for German nationals
+ Given that a user accesses the website of Lime Home
+ When the user is on the landing page
+ When the user provides the last name 
+ When the user provides a 10 digit alphanumeric booking reference code
+ When the information provided are valid as in database
+ When the user submits the form
+ When the user is navigated away from the landing page to the check-in page where the full booking information is displayed
+ When the user selects German as nationality
+ When the user provides the date of birth 
+ When the user selects the type of stay
+ When the information provided are valid
+ Then the user can check-in successfully 

#### Scenario: Check-in for Non-German nationals
+ Given that a user accesses the website of Lime Home
+ When the user is on the landing page
+ When the user provides the last name 
+ When the user provides a 10 digit alphanumeric booking reference code
+ When the information provided are valid as in database
+ When the user submits the form
+ When the user is navigated away from the landing page to the check-in page where the full booking information is displayed
+ When the user selects a nationality other than German
+ When the user provides the date of birth
+ When the user provides the passport number
+ When the user uploads the passport copy
+ When the user selects the type of stay
+ When the information provided are valid
+ Then the user can check-in successfully 

## Task #2: Finding Defects
+ Generally, the code doesn't have unique names for the web elements (CSS id, classes, attributes, etc), this leaves me with no option than to use the auto-generated names assigned to the elements.

### Bugs on Landing Page

1. Non alphanumeric digits still returns a booking information

2. Booking code that are less than 10 or greater than 10 characters in length still pulls up booking information

3. Using special characters as booking code works. For instance using "**VL&$#@**" still returns a booking

4. Names are not validated on Landing Page for booking retrievals - Using numbers as last name was allowed to retrieve bookings

5. There is no German translation of the website on the landing page

### Bugs on Check-in Page:

1. There is no validation of Names and street addresses, Names could be one letter or even a number, same as streets and the user can still go ahead to submit or check in

2. There is no check for postal codes, as such, they could be wrong. One single letter works, whether alphabet or number. For instance:  A user who claims to live in Germany could type in  just one digit and this is regarded as a valid postal code

3. Errors are not highlighted.

... For instance: When a German client neither provides a date of birth nor specify the type of stay (the default status when a user retrieves booking) and have fulfilled all other requirements, the error message says “Please double-check the highlighted fields” without actually highlighting anything for the client to quickly correct it. However, if a user omits the name field, it is highlighted.  

... Similarly, when a non-German checks in and provides a passport number without uploading a picture after fulfilling all other requirement, the error message says “Please double-check the highlighted fields” without actually highlighting anything.

4. The list of countries are not in alphabetical order, a user would experience difficulty selecting a country.

5. There is a partial translation on this page, this translation works only on the date

6. No age restriction in the booking, a child that is 1 year old could check in as well as an unborn child, a user could select 2023 as the date of birth.

7. In my opinion, the user should only be able to pull up their information and not edit their age, names and other information with which they booked. There should be a possibility to request for a change of information of the person  checking in, if necessary. So, the fact that the age doesn’t get pulled up from the database might seem to be a bug, same as a users nationality. I am not 100% sure of this, but this is what I think.

## Task #3: Defect Reporting

I have used Jira to report bug/defect in my previous jobs.  When I create a bug ticket, usually, Jira automatically assigns prefix code to such tickets. The below title could be one of my bug ticket title: **BM1394 - Names are not validated for Check-in**

...Within the text field, I document the procedure to reproduce the bug. Example of such step-by-step bug reproduction is:

+ Given that a user is on the check-in page:
+ When the user writes some Gibberish for names, for instance typing **%** as first name and **&** as last name.
+ The system still accepts the gibberish as being valid

Then, I am supposed to explain or document the proper way the system should behave to help the developer fix the code quickly:<br />

**Expected behaviour**: 
+ Names should be validated (with RegEx)

## Task #4: Automation (Bonus)
The test was implemented with Cypress Test Framework.

