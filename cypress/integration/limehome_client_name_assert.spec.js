describe('Verify that the name on the Landing page is same as that on the Check-in page', () => {
    //create a variable to store the name, initializing it to nothing
    let client_name=""
    it('accesses the host on https://limehome-qa-task.herokuapp.com', () => {
        //Visit the URL of the service to retrieve booking
        cy.visit('https://limehome-qa-task.herokuapp.com')
        //Assert that we are on the Landing page... One could also add a check for the image on the landing page
        cy.get(".header .section-title").should("contain", 'Retrieve your booking')
        //Fetch the text input field for Last Name and type the name
        cy.get(`input[formcontrolname="lastName"]`).type('Donald')
        //Reassign variable with the value of the last name
        client_name = cy.get(`input[formcontrolname="lastName"]`)
        //Provide the alphanumeric booking code of 10 digit
        cy.get(`input[formcontrolname="bookingReference"]`).type('LH978MSQ36')
        //Submit the form to retrieve the booking
        cy.get(".col-form-4  > button > span").contains("Submit").click();
        //Verify that we have been navigated away from the Landing Page to the the Check-in page
        cy.get('.form-header .header').invoke('text').should('contain', 'Your booking details')
        //Verify that the last name on the Landing Page is same as on the Check-in Page
        cy.get('.mat-form-field-infix > input[ng-reflect-model]').eq(1).invoke('text').should('contain', client_name)
    })
})
